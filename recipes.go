package main

import (
	"errors"

	_ "github.com/lib/pq"
)

/*
On this file will be executed the database managment
*/

//Recipe es la estructura de las recetas de cocina
type Recipe struct {
	ID     string `json:"id"`
	Name   string `json:"name"`
	Ingred string `json:"ingred"`
	Prepar string `json:"prepar"`
	active bool   `json:"active"`
}

//Recipes es el arreglo de todas las recetas de cocina de la base de datos
var Recipes []Recipe

//Database managment

func recipeCreate(recipe Recipe) error {
	petition := `INSERT INTO 
					recetas (name, ingr, prep, active)
					VALUES($1, $2, $3, $4)`
	db := getConnection()
	//close the conection
	defer db.Close()

	//U do this to avoid sql inyection
	stmt, err := db.Prepare(petition)
	if err != nil {
		return err
	}
	//close the conection
	defer stmt.Close()

	//Use this to catch any error on the excecution of ur sql petition
	//U use Exec for the INSERT, DELET and UPDATE
	r, err := stmt.Exec(recipe.Name, recipe.Ingred, recipe.Prepar, recipe.active)
	if err != nil {
		return err
	}
	//Se omite el error debido a que es casi imposibe que ocurra
	i, _ := r.RowsAffected()
	if i != 1 {
		return errors.New("Error: Se esperaba solo afectar una fila")
	}

	return nil
}

func recipeDelete(id string) error {
	petition := `DELET FROM
					recetas WHERE id = $1`
	db := getConnection()
	defer db.Close()

	stmt, err := db.Prepare(petition)
	if err != nil {
		return err
	}
	defer stmt.Close()

	r, err := stmt.Exec(id)
	if err != nil {
		return err
	}

	i, _ := r.RowsAffected()
	if i != 1 {
		return errors.New("Error: Se esperaba solo afectar una fila")
	}

	return nil
}

func recipeUpdate(recipe Recipe, id int) error {
	petition := `UPDATE recetas
					SET name = $1, ingr = $2, prep = $3, active = $4
					WHERE id = $5`
	db := getConnection()
	defer db.Close()

	stmt, err := db.Prepare(petition)
	if err != nil {
		return err
	}
	defer stmt.Close()

	r, err := stmt.Exec(recipe.Name, recipe.Ingred, recipe.Prepar, recipe.active, id)
	if err != nil {
		return err
	}
	i, _ := r.RowsAffected()
	if i != 1 {
		return errors.New("Error: Se esperaba solo afectar una fila")
	}

	return nil
}

//Database queries

//Consultar, busca la informacion de las recetas (todas)
func recipesGet() (recipes []Recipe, err error) {
	petition := `SELECT id, name, ingr, prep, active
					FROM recetas`
	db := getConnection()
	defer db.Close()

	rows, err := db.Query(petition)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		recipe := Recipe{}
		err := rows.Scan(&recipe.ID, &recipe.Name, &recipe.Ingred, &recipe.Prepar, &recipe.active)
		if err != nil {
			return nil, err
		}
		Recipes = append(Recipes, recipe)
	}
	return Recipes, nil
}

//Consultar, busca la informacion de las recetas por nombre
func recipeGet(name string) (recetas []Recipe, err error) {
	petition := `SELECT id, name, ingr, prep, active
					FROM recetas
					WHERE name =1$`
	db := getConnection()
	defer db.Close()

	rows, err := db.Query(petition)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		recipe := Recipe{}
		err := rows.Scan(&recipe.ID, &recipe.Name, &recipe.Ingred, &recipe.Prepar, &recipe.active)
		if err != nil {
			return nil, err
		}
		Recipes = append(Recipes, recipe)
	}
	return Recipes, nil
}
