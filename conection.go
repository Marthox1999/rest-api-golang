package main

import (
	"database/sql"
	"log"

	_ "github.com/lib/pq"
)

/*
On this file will be execute the conection with the database
*/

// getConnection obtiene una conexion a la base de datos
func getConnection() *sql.DB {
	dsn := "postgres://golang:golang@127.0.0.1:5432/goapi?sslmode=disable"
	db, err := sql.Open("postgres", dsn)
	if err != nil {
		log.Fatal(err)
	}
	return db
}
