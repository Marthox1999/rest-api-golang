package main

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

/*
On this file will be executed the rest api
*/

//Network request managments

//This function allow us to obtain all the recipes
func getAllRecipes(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")
	Recipes, err := recipesGet()
	if err != nil {
		json.NewEncoder(response).Encode(nil)
	}
	json.NewEncoder(response).Encode(Recipes)
}

//Search recipes by name
func getRecipe(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")
	params := mux.Vars(request) //Obtain the params
	// Loop through books and find with the id
	for _, item := range Recipes {
		if item.Name == params["Name"] {
			json.NewEncoder(response).Encode(item)
			return
		}
	}
	json.NewEncoder(response).Encode(&Recipe{})
}

func createRecipe(response http.ResponseWriter, request *http.Request) {

}

func deleteRecipe(response http.ResponseWriter, request *http.Request) {
}

func updateRecipe(response http.ResponseWriter, request *http.Request) {
}

func main() {
	//Init the router
	router := mux.NewRouter()
	router.HandleFunc("/Recetas", getAllRecipes).Methods("GET")
	router.HandleFunc("/Recetas/{id}", getRecipe).Methods("GET")
	router.HandleFunc("/Recetas/{id}", createRecipe).Methods("POST")
	router.HandleFunc("/Recetas/{id}", deleteRecipe).Methods("Delete")
	log.Fatal(http.ListenAndServe(":8081", router))
}
